import os
from PIL import Image


from PIL import ImageSequence

import imageio
import os
from PIL import Image, ImageSequence



def iter_frames(im):
    try:
        i= 0
        while 1:
            im.seek(i)
            imframe = im.copy()
            if i == 0:
                palette = imframe.getpalette()
            else:
                imframe.putpalette(palette)
            yield imframe
            i += 1
    except EOFError:
        pass
def compressGif(filename):
    print(filename)
    gif = Image.open(filename)
    if not gif.is_animated:
        return False
    print(filename[:-3] + '-c.gif')
    imageio.mimsave(filename[:-3] + '-c.gif', [frame.convert('RGB') for frame in ImageSequence.Iterator(gif)],
                    duration=gif.info['duration'] / 2000)

folder = os.path.join(os.path.dirname(__file__), 'emoji')
for i in os.listdir(folder):
    # print(i)


    # compressGif(os.path.join(folder, i))
    # break
    im = Image.open(os.path.join(folder, i))
    im = im.convert('RGBA')
    for frame in iter_frames(im):
        frame.save(os.path.join(folder, os.pardir, 'emoji-thum', f'{i[:-3]}png'.replace(' ', '')),**frame.info)
    #     break
