import json
import re

with open('code.json', encoding='utf-8') as f:
    data = json.loads(f.read())

def parse(s):
    l = []
    ps = re.findall(r'\(\(.*?\)\)', s)
    for p in ps:
        ll = []
        points = p[2:-2].split(',')
        for point in points:
            point = point.split()
            ll.append({'longitude': float(point[0].replace('(', '').replace(')', '')), 'latitude': float(point[1].replace('(', '').replace(')', ''))})
        l.append(ll)
    return l


d = {}
for i in data:
    if i['parent'] not in d:
        d[i['parent']] = {'data': {}}

    d[i['parent']]['data'][str(i['code'])] = parse(i['st_astext'])


for k, v in d.items():
    with open(f'./data/{k}.json', 'w', encoding='utf-8') as f:
        f.write(json.dumps(v))

print(1)